import java.util.Random;

public class Copo
{
    private int um;
    private int dois;
    private int trez;
    private int quatro;
    private int cinco;
    Random random = new Random();
    
    public Copo() {
        this.um = 0;
        this.dois = 0;
        this.trez = 0;
        this.quatro = 0;
        this.cinco = 0;
    }
    
    public void setUm(int uno) {
        this.um = uno;
    }
    
    public int getUm() {
        return this.um;
    }
    
    public void setDois(int dos) {
        this.dois = dos;
    }
    
    public int getDois() {
        return this.dois;
    }
    
    public void setTrez(int teis) {
        this.trez = teis;
    }
    
    public int getTrez() {
        return this.trez;
    }
    
    public void setQuatro(int quato) {
        this.quatro = quato;
    }
    
    public int getQuatro() {
        return this.quatro;
    }
    
    public void setCinco(int cincu) {
        this.cinco = cincu;
    }
    
    public int getCinco() {
        return this.cinco;
    }
        
    public int zeraDado(int opc) {
        
        int result;
        
        switch(opc){
     
            case 1:
                this.um = 0;
                result = 1;
                break;
            case 2:
                this.dois = 0;
                result = 1;
                break;
            case 3:
                this.trez = 0;
                result = 1;
                break;
            case 4:
                this.quatro = 0;
                result = 1;
                break;
            case 5:
                this.cinco = 0;
                result = 1;
                break;
            default:
                result = 0;
                break; 
        }
        return result;
    }
    
    public int jogaDado(int dado) {
        
        int result = 0;
        
        switch(dado) {
                        
            case 1:
                if (this.um == 0){
                    this.um = random.nextInt(5)+1;
                    result = 1;
                }else{
                    result = 0;
                }
                break;
            case 2:
                if (this.dois == 0){
                    this.dois = random.nextInt(5)+1;
                    result = 1;
                }else{
                    result = 0;
                }
                break;
            case 3:
                if(this.trez == 0) {
                    this.trez = random.nextInt(5)+1;
                    result = 1;
                }else{
                    result = 0;
                }
                break;
            case 4:
                if(this.quatro == 0){
                    this.quatro = random.nextInt(5)+1;
                    result = 1;
                }else{
                    this.quatro = 0;
                }
                break;
            case 5:
                if(this.cinco == 0){
                    this.cinco = random.nextInt(5)+1;
                    result = 1;
                }else{
                    result = 0;
                }
                break;
            default:
                result = 0;
                break;
        }
        return result;
    }
}
