public class Perfil
{
   private String nome;
   private int empate;
   private int vitoria;
   private int derrota;
   private int total;
   private int partidas;
   
   
   public void setNome(String name) {
       this.nome = name;
    }
    
    public String getNome() {
        return this.nome;
    }
   
    public void setEmpate(int tie) {
        this.empate = tie;
    }
   
    public int getEmpate() {
        return this.empate;
    }
    
    public void setVitoria(int win) {
        this.vitoria = win;
    }
   
    public int getVitoria() {
        return this.vitoria;
    }
   
    public void setDerrota(int lose) {
        this.derrota = lose;
    }
   
    public int getDerrota() {
        return this.derrota;
    }
    
    public void setTotal(int tot){
        this.total = tot;
    }
    
    public int getTotal(){
        return this.total;
    }
    
    public void setPartidas(int games){
        this.partidas = games;
    }
    
    public int getPartidas(){
        return this.partidas;
    }
}
