public class Jogador
{
   private String nome;
   private int pontos;
     
   public Jogador() {
       this.pontos = 0;  
    }
    
    public void setNome(String name) {
        this.nome = name;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setPontos(int points) {
        this.pontos = points;
    }
    
    public int getPontos() {
        return this.pontos;
    }
}
