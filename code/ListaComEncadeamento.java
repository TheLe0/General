public class ListaComEncadeamento
{
     private int tamanho = 0;
    private int capacidade ;
    private Nodo inicio = null ;
    private Nodo fim = null ;
    
    
     public ListaComEncadeamento(int capacidade)
    {
        this.capacidade = capacidade ;
    }
    
      public ListaComEncadeamento()
    {
        this.capacidade = 10 ;
    }
    
      public int tamanho()
    {
        return tamanho ;
    }
    
     public Perfil pesquisa(Perfil obj)
    {
        Nodo temp = inicio ;
        while(temp!=null)
        {
            if(temp.getInfo()==(obj))
            {
                return temp.getInfo();
            }           
            temp = temp.getProximo();
        }
        return null;
    }
    
     public int incluir(Perfil obj)    
    {
        if(tamanho==capacidade)
        {
            return 1;
        }
        if(this.pesquisa(obj)!=null)
        {
            return 2;
        }       
        Nodo temp = new Nodo();
        temp.setInfo(obj);
        if(tamanho==0)
        {
            inicio = temp ;
        }
        else
        {
            fim.setProximo(temp);
        }
        fim = temp;
        return 0;
    }
    
    
    public Perfil get(int indice)
    {
        if(indice>=0 && indice<tamanho)
        {
            Nodo temp = inicio;
            for(int i=0 ; i<indice ;i++)
            {
                temp = temp.getProximo();
            }
            return temp.getInfo();
        }
        return null;
    }
    
       private class Nodo
    {
        private Perfil info ;
        private Nodo proximo ;

        public void setInfo(Perfil obj)
        {
            this.info = obj;
        }

        public Perfil getInfo()
        {
            return this.info ;
        }

        public void setProximo(Nodo nodo)
        {
            this.proximo = nodo;
        }

        public Nodo getProximo()
        {
            return this.proximo ;
        }
    }
}
