# General
Projeto da disciplina de Programação Orientada a Objetos, 2018/2 . Professor Marcos Eduardo Casa.
O jogo de General consiste de um certo número de rodadas: em cada uma delas, cada jogador, por sua vez, joga os dados 3 vezes e conforme o resultado obtido, marca uma das jogadas previstas em sua cartela. Uma vez marcada, aquela jogada não pode ser repetida pelo mesmo jogador até o final da partida.

Regras
Lembrando que vou colocar aqui as regras que utilizei, para variações podem entrar em contato comigo por nossos canais de comunicação!

(1) Sendo 11 o número de jogadas possíveis e 13 o número de linhas de cada coluna na cartela de marcação, um jogo consiste de 13 rodadas, ou 13 jogadas para cada jogador.

(2) Cada jogador, em sua vez, tem três chances de arremessar os dados. Na primeira, joga os cinco dados; na segunda, conforme o resultado obtido, pode voltar a arremessar de um a cinco dados, mantendo os demais sobre a mesa, ou aceitar o resultado, dando a jogada por encerrada; na terceira, da mesma forma, pode arremessar de um a cinco dados (mesmo os que ele tenha mantido sobre a mesa entre o primeiro e o segundo arremesso) ou aceitar o resultado.

(3) O resultado obtido ao final de três arremessos (uma jogada completa) deve ser classificado, pelo próprio jogador, como uma das seguintes 13 possibilidades:

Jogada de 1: um certo número de dados (de 0 a 5) marcando o número 1; sendo que a jogada vale mais pontos conforme a quantidade de dados que marcarem o número 1. Por exemplo: 1-1-1-4-5 vale 3 pontos.
Jogadas de 2, 3, 4, 5 e 6: correspondentes à jogada de 1 para os demais números. Por exemplo: 3-3-4-4-5 vale 6 pontos se for considerada uma jogada de 3; ou 8 pontos se for considerada uma jogada de 4; ou ainda 5 pontos se for uma jogada de 5.
Sequência: Pode ser feita de 3 maneiras: 1-2-3-4-5 / 2-3-4-5-6 / 3-4-5-6-1. Vale 20 pontos ou 25 se for de cara*.
Fula ou Full-house: uma trinca e um par (exemplo: 2-2-2-6-6). Vale 30 pontos ou 35 se for de cara*.
Four ou Quadra: quatro dados marcando o mesmo número. Exemplo: 1-5-5-5-5 ? Vale 40 pontos ou 45 se for de cara*.
General ou Yam: cinco dados marcando o mesmo número (por exemplo: 4-4-4-4-4). Vale 50 pontos ou Vence a partida se for de cara*.
Jogada aleatória (Regra retirada do Jogo Yam): qualquer combinação. Vale a soma dos 5 dados. Por exemplo: 1-4-4-5-6 vale 20 pontos.
(4) O resultado é marcado na cartela, na coluna do jogador e na linha correspondente à jogada. Aquela linha (e portanto aquela jogada) não poderá mais ser utilizada pelo jogador na mesma partida.

(5) Se um determinado resultado não puder ser classificado como nenhuma das jogadas ainda restantes para aquele jogador, ele deverá escolher qual das jogadas restantes será descartada, marcando 0 (zero) na linha correspondente.

(6) Ao final de 13 rodadas, com a cartela toda preenchida, somam-se os valores de cada coluna, e o jogador que obtiver mais pontos será considerado o vencedor.

de cara*: Fazer a pontuação direto, na primeira tentativa!
